# Assignment 2

## Description

NodeJS console application for:

1. Read file
2. OS info display
3. Start HTTP server

In the project directory, you can run: `npm start`.

### Read file (option 1)

Displays the content of package.json in the console.

### OS info display (option 2)

Displays information about the operating system.

### HTTP server (option 3)

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
