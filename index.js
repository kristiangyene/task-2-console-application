const ReadJsonFile = () => {
    console.log('Reading file...');

    let readline = require('readline');
    let fs = require('fs');

    // Read every line in file
    let interface = readline.createInterface({
        input: fs.createReadStream('package.json')
    });
    interface.on('line', line => console.log(line));
}

const DisplayOsInfo = () => {
    console.log('Displaying info...');

    // Extracting OS info
    const os = require('os');
    const totalMemory = (os.totalmem() / 1073741824).toFixed(2);
    console.log(`SYSTEM MEMORY: ${totalMemory} GB`);

    const freeMemory = (os.freemem() / 1073741824).toFixed(2);
    console.log(`FREE MEMORY: ${freeMemory} GB`);

    const cpuCount = Object.keys(os.cpus()).length;
    console.log(`CPU CORES: ${cpuCount}`);

    console.log(`ARCH: ${os.arch()}`);
    console.log(`PLATFORM: ${os.platform()}`);
    console.log(`RELEASE: ${os.release()}`);

    const {username} = os.userInfo();
    console.log(`USER: ${username}`);
}

const StartHTTPServer = () => {
    console.log('Starting server...');
    const { PORT = 3000 } = process;

    const http = require('http').createServer(function(req, res){
        res.setHeader('Content-Type', 'text/plain');
        return res.end('Hello world');

    });
    http.listen(PORT, () => console.log("server running at PORT ", PORT));
}


let options = { 
    // Associated function for user cmd
    'Read package.json': ReadJsonFile,
    'Display OS info': DisplayOsInfo,
    'Start HTTP server': StartHTTPServer
}


let inquirer = require('inquirer');
// Display prompt for user options
inquirer.prompt([{
      type: 'rawlist',
      name: 'list-prompt',
      message: 'Choose an option:\n',
      choices: Object.keys(options)
    }
  ])

  .then(answers => {
    let answer = answers['list-prompt'];
    answer = options[answer];
    answer();
  })

  .catch(error => console.error(error));



